/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 7
*/
const port=5000;
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.listen(port,()=>{
  console.log(`Server started on ${port} port...`);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':true}));




//Дополнительное задание
const checkFunct = function(req,res,next){
  if (!req.headers.header||req.headers.header!='Key'){
    return res.status(401).send('Error 401');
  }
  next();
}

app.post('/post', checkFunct,(req,res)=>{
  if (Object.keys(req.body).length>0){
    res.json(req.body);
  } else {res.status(404).send('404 Not found');}
});


app.get('/', (req,res)=>{
  res.status(200).send('Hello, Express.js');
});

const sub = function (req,res){
  res.status(200).send('You requested URI: '+req.url);
}

app.all(/\/sub\/[0-9a-z]+(\/[0-9a-z]+)?$/, sub);


app.get('/hello', (req,res)=>{
  res.status(200).send('Hello stranger!');
});

app.all('/*', (req,res)=>{
  res.status(404).send('Method not found');
});
